package ru.t1.bugakov.tm.api;

import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public interface ITaskService  {

    Task create(String name, String description);

    void add(Task task);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void clear();

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
