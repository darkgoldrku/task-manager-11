package ru.t1.bugakov.tm.api;

import ru.t1.bugakov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void add(Project project);

    Project create(String name, String description);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void clear();

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
