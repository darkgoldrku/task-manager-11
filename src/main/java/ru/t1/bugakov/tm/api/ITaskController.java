package ru.t1.bugakov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();
}
