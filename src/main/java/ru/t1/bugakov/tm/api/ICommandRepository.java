package ru.t1.bugakov.tm.api;

import ru.t1.bugakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
