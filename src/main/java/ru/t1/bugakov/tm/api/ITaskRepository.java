package ru.t1.bugakov.tm.api;

import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void clear();

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
