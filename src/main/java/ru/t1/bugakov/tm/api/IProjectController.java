package ru.t1.bugakov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

}